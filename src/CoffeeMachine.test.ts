import {CoffeeMachine, DrinkMaker} from './CoffeeMachine'
import mock = jest.mock;

describe('CoffeeMachine', () => {
  it('Should make a tea', () => {
    const drinkMaker: DrinkMaker = {
      make: jest.fn()
    }
    const coffeeMachine = new CoffeeMachine(drinkMaker)
    coffeeMachine.tea()
    coffeeMachine.make()

    expect(drinkMaker.make).toHaveBeenCalledTimes(1)
    expect(drinkMaker.make).toBeCalledWith('T::')
  })
  it('Should make a Coffee', () => {
    const drinkMaker: DrinkMaker = {
      make: jest.fn()
    }
    const coffeeMachine = new CoffeeMachine(drinkMaker)
    coffeeMachine.coffee()
    coffeeMachine.make()

    expect(drinkMaker.make).toHaveBeenCalledTimes(1)
    expect(drinkMaker.make).toBeCalledWith('C::')
  })
  it('Should make a Chocolate', () => {
    const drinkMaker: DrinkMaker = {
      make: jest.fn()
    }
    const coffeeMachine = new CoffeeMachine(drinkMaker)
    coffeeMachine.chocolate()
    coffeeMachine.make()

    expect(drinkMaker.make).toHaveBeenCalledTimes(1)
    expect(drinkMaker.make).toBeCalledWith('H::')
  })
  xit('Should make a Tea with 1 sugar', () => {
    const drinkMaker: DrinkMaker = {
      make: jest.fn()
    }
    const coffeeMachine = new CoffeeMachine(drinkMaker)
    coffeeMachine.tea()
    coffeeMachine.sugar()
    coffeeMachine.make()

    expect(drinkMaker.make).toHaveBeenCalledTimes(1)
    expect(drinkMaker.make).toBeCalledWith('T:1:')
  })
})
