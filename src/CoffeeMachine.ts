export interface DrinkMaker {
  make(command: string)
}
enum drinks {
  Tea,
  Coffee,
  Chocolate,
}
export class CoffeeMachine {
  private drink: drinks
  private sugarAdded = 0

  constructor(private drinkMaker: DrinkMaker) {
  }

  public tea() {
    this.drink = drinks.Tea
  }

  public make() {
    const message = this.drinkPart();
    this.drinkMaker.make(`${message}::`)
  }

  public coffee() {
    this.drink = drinks.Coffee
  }

  public chocolate() {
    this.drink = drinks.Chocolate
  }

  public sugar() {
    this.sugarAdded = 1
  }
  private drinkPart(): string {

    switch (this.drink) {
      case drinks.Tea: return 'T'
      case drinks.Coffee: return 'C'
      case drinks.Chocolate: return 'H'
      default:
        throw Error()
    }
  }
}
